package mooc.vandy.java4android.calculator.logic;

/**
 * Perform the Multiply operation.
 */
public class Multiply implements Operation {
    // TODO -- start your code here
    public String calculate(int val1, int val2){
        //perform operation
        int answer = val1 * val2;
        //convert to string
        String retString = answer + "";
        return retString;
    }
}
