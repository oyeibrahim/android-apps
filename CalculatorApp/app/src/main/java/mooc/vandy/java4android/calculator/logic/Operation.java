package mooc.vandy.java4android.calculator.logic;

/**
 * Created by Abu 'Abdur-Rahman on 27/07/2019.
 */
public interface Operation {
    public String calculate(int val1, int val2);
}
