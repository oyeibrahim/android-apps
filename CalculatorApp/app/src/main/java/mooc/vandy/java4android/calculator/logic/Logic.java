package mooc.vandy.java4android.calculator.logic;

import mooc.vandy.java4android.calculator.logic.Add;
import mooc.vandy.java4android.calculator.logic.Divide;
import mooc.vandy.java4android.calculator.logic.Multiply;
import mooc.vandy.java4android.calculator.logic.Subtract;
import mooc.vandy.java4android.calculator.ui.ActivityInterface;
import mooc.vandy.java4android.calculator.ui.MainActivity;

/**
 * Performs an operation selected by the user.
 */
public class Logic implements LogicInterface {
    /**
     * Reference to the Activity output.
     */
    protected ActivityInterface mOut;

    /**
     * Constructor initializes the field.
     */
    public Logic(ActivityInterface out){
        mOut = out;
    }

    /**
     * Perform the @a operation on @a argumentOne and @a argumentTwo.
     */
    public void process(int argumentOne,
                        int argumentTwo,
                        int operation){
        // TODO -- start your code here


        Operation addition = new Add();
        Operation subtraction = new Subtract();
        Operation multiplication = new Multiply();
        Operation division = new Divide();

        String result;

        if (operation == 1){
            result = addition.calculate(argumentOne, argumentTwo);
        }else if (operation == 2){
            result = subtraction.calculate(argumentOne, argumentTwo);
        }else if (operation == 3){
            result = multiplication.calculate(argumentOne, argumentTwo);
        }else {
            result = division.calculate(argumentOne, argumentTwo);
        }

        mOut.print(result);

    }
}
