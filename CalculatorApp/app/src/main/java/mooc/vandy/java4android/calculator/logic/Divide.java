package mooc.vandy.java4android.calculator.logic;

/**
 * Perform the Divide operation.
 */
public class Divide implements Operation {
    // TODO -- start your code here
    public String calculate(int val1, int val2){
        //perform operation
        int answer = val1 / val2;
        //get remainder
        int remainder = val1 % val2;
        //convert to string make it the right format
        String retString = answer + " R: " + remainder;
        return retString;
    }
}
